# NossaConexão - Canal para compartilhamento de músicas

Requisitos obrigatórios:

- [ x ] Login e senha (usar salt ou password_hash do php)

- [ x ] Ao se cadastrar deverá possuir papel de usuário simples (mínimo 1 CRU ou CR)

- [ x ] O admin já deve ser pré-cadastrado com o login admin e senha admin (deve ser no banco e não hard-code)

- [ x ] Envio de imagens ou músicas

- [ x ] Armazenar imagens de forma segura (banco ou arquivo)

- [ x ] Armazenar imagens de forma otimizada (arquivo)

- [ x ] Utilizar SPA no sistema privado

- [ x ] Utilizar um editor de HTML

- [ x ] Utilizar datas e Timezone
