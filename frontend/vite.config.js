import react from "@vitejs/plugin-react"
import { defineConfig } from "vite"

import * as path from "path"

export default defineConfig({
  resolve: {
    alias: [
      { find: "@", replacement: path.resolve(__dirname, "src") }
    ],

  },
  plugins: [react()],
  server: {
    host: true,
  },
  build: {
    chunkSizeWarningLimit: 1600
  }
})
