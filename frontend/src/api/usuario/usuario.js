import { axiosApi } from "@/lib/axios"

const controller = "UsuarioController"

export const signUp = async (user) => {
    const { data: usuario } = await axiosApi.post("api.php", user, {
        params: {
            action: "createUsuario",
            controller
        }
    })
    return usuario
}

export const signIn = async (credentials) => {
    const { data: token } = await axiosApi.post("api.php", credentials, {
        params: {
            action: "login",
            controller
        }
    })
    return token
}

export const signOut = async () => {
    const { data: logout } = await axiosApi.post("api.php", {}, {
        params: {
            action: "logout",
            controller
        }
    })
    return logout
}