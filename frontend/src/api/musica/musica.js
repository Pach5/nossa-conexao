import { axiosApi } from "@/lib/axios"

const controllers = {
    "musica": "MusicaController",
    "artista": "ArtistaController",
    "arquivo": "ArquivoController"
}

export const saveArtista = async (artista) => {
    const { data: savedArtista } = await axiosApi.post("api.php", artista, {
        params: {
            action: "createArtista",
            controller: controllers["artista"]
        }
    })
    return savedArtista
}

export const saveImagem = async (imagem) => {
    const { data: savedImagem } = await axiosApi.post("api.php", imagem, {
        params: {
            action: "uploadImagem",
            controller: controllers["arquivo"]
        }
    })
    return savedImagem
}

export const saveAudio = async (audio) => {
    const { data: savedAudio } = await axiosApi.post("api.php", audio, {
        params: {
            action: "uploadAudio",
            controller: controllers["arquivo"]
        }
    })
    return savedAudio
}

export const saveMusica = async (musica) => {
    const { data: savedMusica } = await axiosApi.post("api.php", musica, {
        params: {
            action: "createMusica",
            controller: controllers["musica"]
        }
    })
    return savedMusica
}

export const fetchArtistas = async () => {
    const { data: artistas } = await axiosApi.get("api.php", {
        params:
        {
            action: "findAllArtistas",
            controller: controllers["artista"]
        }
    })
    return artistas
}

export const fetchMusicas = async () => {
    const { data: musicas } = await axiosApi.get("api.php", {
        params:
        {
            action: "findAllMusicas",
            controller: controllers["musica"]
        }
    })
    return musicas
}

export const deleteMusica = async (musica) => {
    const { data: deletedObj } = await axiosApi.post("api.php", musica, {
        params:
        {
            action: "deleteMusica",
            controller: controllers["musica"]
        },
    })
    return deletedObj
}

export const fetchMusicaById = async (musica) => {
    const { data: fetchedMusica } = await axiosApi.post("api.php", musica, {
        params:
        {
            action: "findOneMusica",
            controller: controllers["musica"]
        },
    })
    return fetchedMusica
}

export const updateMusica = async (musica) => {
    const { data: fetchedMusica } = await axiosApi.post("api.php", musica, {
        params:
        {
            action: "updateMusica",
            controller: controllers["musica"]
        },
    })
    return fetchedMusica
}