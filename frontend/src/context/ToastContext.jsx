import { createContext, useContext, useState } from "react"
import CustomToast from "@/components/ui/CustomToast"

const ToastContext = createContext()

export function ToastProvider({ children }) {
    const [toast, setToast] = useState(null)

    const showToast = (type, title, message) => {
        setToast({ type, title, message })
        setTimeout(hideToast, 5000) // Esconde o toast após 5 segundos
    }

    const hideToast = () => {
        setToast(null)
    }

    return (
        <ToastContext.Provider
            value={{ toast, showToast, hideToast }}
        >
            {children}
            <CustomToast /> {/* Renderiza o componente CustomToast em qualquer lugar */}
        </ToastContext.Provider>
    )
}

export function useToast() {
    return useContext(ToastContext)
}
