import { Outlet } from "react-router-dom"
import Navbar from "@/components/public/PublicNavbar"

export default function PublicLayout() {

  return (
    <div style={{ minHeight: "100vh", display: "flex", flexDirection: "column" }}>
      <header>
        <Navbar />
      </header>
      <main style={{ flexGrow: 1, paddingBottom: "70px" }}>
        <Outlet />
      </main>
    </div>

  );
}

