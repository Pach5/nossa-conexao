import { Outlet } from "react-router-dom"
import Navbar from "@/components/private/PrivateNavbar"

export default function PrivateLayout() {

  return (
    <>
      <header>
        <Navbar />
      </header>
      <main className="align-items-center p-5 gap-5 w-100 h-100 d-flex flex-column">
        <Outlet />
      </main>
    </>

  );
}

