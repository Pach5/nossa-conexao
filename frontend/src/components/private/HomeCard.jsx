import Card from 'react-bootstrap/Card'

function HomeCard({ title, body, icon, button }) {
    return (
        <Card style={{ width: '300px' }}>
            <Card.Body className='d-flex flex-column justify-content-between'>
                <Card.Title className='d-flex align-items-center gap-2'>
                    {icon}
                    {title}
                </Card.Title>
                <Card.Text>
                    {body}
                </Card.Text>
                {button}
            </Card.Body>
        </Card>
    )
}

export default HomeCard