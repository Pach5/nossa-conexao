import Button from 'react-bootstrap/Button'
import Card from 'react-bootstrap/Card'

function WelcomeCard({ username }) {
    return (
        <Card>
            <Card.Header>Boas vindas!</Card.Header>
            <Card.Body>
                <Card.Title>Olá, {username}!</Card.Title>
                <Card.Text>
                    Estamos empolgados em ter você como nosso administrador da batida. Compartilhar músicas que mexem com nossas almas é o que fazemos de melhor, e agora, com você a bordo, temos certeza de que a sintonia ficará ainda mais incrível.
                </Card.Text>

            </Card.Body>
        </Card>
    );
}

export default WelcomeCard;