import { Navbar, Nav, Container, Button } from "react-bootstrap"
import { useNavigate } from "react-router-dom"
import { useSession } from "@/hooks/useSession"

export default function PrivateNavbar() {

    const user = useSession()
    const navigate = useNavigate()

    const handleSignOut = async () => {
        sessionStorage.removeItem("token")
        navigate("/login")
    }


    return (
        <Navbar expand="lg" className="bg-body-tertiary">
            <Container>
                <Navbar.Brand className="d-flex gap-2" href={`/${user?.role}`}>
                    <img
                        src="/music.png"
                        width="30"
                        height="30"
                        className="d-inline-block align-top"
                        alt="React Bootstrap logo"
                    />
                    Nossa Conexão
                </Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="me-auto w-100 my-2 justify-content-end">
                        <Nav.Link className="mr-3" href="/user/cadastro/musica">+ Música</Nav.Link>
                        <Button onClick={handleSignOut}>Sair</Button>
                    </Nav>
                </Navbar.Collapse>
            </Container>
        </Navbar>
    )
}
