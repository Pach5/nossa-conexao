import { Navbar, Nav, Container } from "react-bootstrap"
import { useSession } from "@/hooks/useSession"

export default function PrivateNavbar() {

    const user = useSession()

    return (
        <Navbar expand="lg" className="bg-body-tertiary">
            <Container>
                <Navbar.Brand className="d-flex gap-2" href="/">
                    <img
                        src="/music.png"
                        width="30"
                        height="30"
                        className="d-inline-block align-top"
                        alt="React Bootstrap logo"
                    />
                    Nossa Conexão
                </Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="me-auto w-100 justify-content-end">

                        {user ? (
                            <Nav.Link href={`/${user.role}`}>Voltar ao Dashboard</Nav.Link>
                        ) :
                            (<>
                                <Nav.Link href="/cadastro">Cadastro</Nav.Link>
                                <Nav.Link href="/login">Login</Nav.Link>
                            </>
                            )}
                    </Nav>
                </Navbar.Collapse>
            </Container>
        </Navbar>
    )
}
