import { useState } from 'react'

import CreatableSelect from 'react-select/creatable'

export default function ArtistaCreatableSelect({
    options, setOptions, createOption,
    artista, setArtista,
    className
}) {

    const [isLoading, setIsLoading] = useState(false)

    const handleCreate = (inputValue) => {
        setIsLoading(true)
        setTimeout(() => {
            const newOption = createOption(inputValue)
            setIsLoading(false)
            setOptions((prev) => [...prev, newOption])
            setArtista(newOption)
        }, 1000)
    }

    return (
        <CreatableSelect
            required
            className={className}
            placeholder={"Selecione um artista..."}
            isClearable
            isDisabled={isLoading}
            isLoading={isLoading}
            onChange={(artista) => setArtista(artista)}
            onCreateOption={handleCreate}
            options={options}
            value={artista}
        />
    )

}