import { useState, useEffect } from "react"
import Button from "react-bootstrap/Button"
import Card from "react-bootstrap/Card"
import { AiOutlineUser } from "react-icons/ai"

export default function MusicaCard({
    musica,
    playingAudio,
    isPlaying,
    setIsPlaying,
    handleMusica
}) {
    const [audio] = useState(new Audio(musica.audio))

    useEffect(() => {
        audio.addEventListener("ended", () => {
            setIsPlaying(false)
        })

        return () => {
            audio.removeEventListener("ended", () => {
                setIsPlaying(false)
            })
        }
    }, [audio])

    return (
        <Card style={{ width: "100%", maxWidth: "600px" }}>
            {musica.imagem && <Card.Img variant="top" src={musica.imagem} />}
            <Card.Header>
                {musica.username === "admin" ? (
                    "Recomendado por nós!"
                ) : (
                    <>
                        <AiOutlineUser /> {musica.username}
                    </>
                )}
            </Card.Header>
            <Card.Body>
                <Card.Title>{musica.titulo}</Card.Title>
                <Card.Subtitle>{musica.artista}</Card.Subtitle>
                <Card.Text className="text-muted mt-2" dangerouslySetInnerHTML={{ __html: musica.texto }} />
                <Button
                    className="px-5 py-3 mt-2 w-100"
                    variant="primary"
                    onClick={() => {
                        handleMusica(musica, audio)
                    }}
                >
                    {playingAudio == audio && isPlaying ? "Pausar música" : "Tocar música"}
                </Button>
            </Card.Body>
        </Card>
    )
}
