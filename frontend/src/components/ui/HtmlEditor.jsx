import React, { useState } from "react"
import ReactQuill from "react-quill"
import "react-quill/dist/quill.snow.css"

const HtmlEditor = ({ texto, setTexto }) => {
    const handleChange = (value) => {
        setTexto(value)
    }

    return (
        <ReactQuill value={texto} onChange={handleChange} />
    )
}

export default HtmlEditor
