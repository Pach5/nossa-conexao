import { ToastContainer, Toast } from "react-bootstrap"
import { useToast } from "@/context/ToastContext"

function CustomToast() {
    const { toast, hideToast } = useToast()

    if (!toast) {
        return null // Se não houver toast, não renderize nada
    }

    const { type, title, message } = toast

    return (
        <ToastContainer className="ml-3 mb-3 position-fixed" position="bottom-start">
            <Toast show={true} onClose={hideToast}>
                <Toast.Header className={`bg-${type}`}>
                    <img
                        src="holder.js/20x20?text=%20"
                        className="rounded me-2"
                        alt=""
                    />
                    <strong className="me-auto text-light">{title}</strong>
                </Toast.Header>
                <Toast.Body >{message}</Toast.Body>
            </Toast>
        </ToastContainer>
    )
}

export default CustomToast
