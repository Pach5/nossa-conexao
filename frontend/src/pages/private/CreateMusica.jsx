import { useEffect, useState } from "react"
import { Form, Button } from "react-bootstrap"
import { useNavigate } from "react-router-dom"
import { useErrorToast } from "@/hooks/useToast"
import ArtistaCreatableSelect from "@/components/ui/ArtistaCreatableSelect"
import {
    fetchArtistas,
    saveArtista,
    saveImagem,
    saveAudio,
    saveMusica
} from "@/api/musica/musica"
import { useSession } from "@/hooks/useSession"
import HtmlEditor from "../../components/ui/HtmlEditor"

function CreateMusica() {

    const navigate = useNavigate()
    const showErrorToast = useErrorToast()

    const user = useSession()

    const [artista, setArtista] = useState({})
    const [nomeMusica, setNomeMusica] = useState("")
    const [texto, setTexto] = useState("")
    const [imagem, setImagem] = useState(null)
    const [audioFile, setAudioFile] = useState(null)

    const [errors, setErrors] = useState([])

    const [options, setOptions] = useState()

    const createOption = (label, value) => ({
        label,
        value
    })

    useEffect(() => {

        const setOptionsSelectArtista = async () => {
            const artistas = await fetchArtistas()
            const options = artistas.map((option) => createOption(option?.nome, option?._id))
            setOptions(options)
        }

        setOptionsSelectArtista()

    }, [])

    const isFormValid = () => {
        setErrors([])

        let isValid = true
        const MAX_FILE_SIZE_40_MB = 40 * 1024 * 1024
        const FILE_ERROR_MESSAGE = "Arquivo muito grande! Aceitamos no máximo 40MB!"

        if (audioFile && audioFile.size > MAX_FILE_SIZE_40_MB) {
            setErrors(prevErrors => ({ ...prevErrors, errorAudio: { message: FILE_ERROR_MESSAGE } }))
            isValid = false
        }

        if (imagem && imagem.size > MAX_FILE_SIZE_40_MB) {
            setErrors(prevErrors => ({ ...prevErrors, errorImagem: { message: FILE_ERROR_MESSAGE } }))
            isValid = false
        }

        if (!artista || Object.keys(artista).length == 0) {
            setErrors(prevErrors => ({ ...prevErrors, errorArtista: { message: "Por favor, selecione um artista!" } }))
            isValid = false
        }
        return isValid
    }

    const handleSubmit = async (e) => {
        e.preventDefault()
        if (!isFormValid()) return

        try {
            //   Se o usuário selecionou "novo artista", insira o novo artista no banco de dados
            let artistaId = null
            if (!artista?.value) {
                const novoArtista = await saveArtista({ nome: artista?.label })
                artistaId = novoArtista._id
            } else {
                artistaId = artista?.value
            }

            //   Envie a imagem e obtenha o ID do arquivo
            let imagemId
            if (imagem) {
                const formDataImagem = new FormData()
                formDataImagem.append("imagem", imagem)
                const savedImagem = await saveImagem(formDataImagem)
                imagemId = savedImagem._id
            }

            //   Envie o arquivo de áudio e obtenha o ID do arquivo
            const formDataAudio = new FormData()
            formDataAudio.append("audio", audioFile)
            const savedAudio = await saveAudio(formDataAudio)
            const audioId = savedAudio?._id

            //   Insira os detalhes da música
            const musica = await saveMusica({
                titulo: nomeMusica,
                texto,
                artistaId,
                arquivoId: audioId,
                fotoId: imagemId,
                usuarioId: user?._id
            })

            if (!musica?._id) return showErrorToast("Falha ao compartilhar música!", "Houve algum erro imprevisto. Por favor, tente novamente mais tarde ou entre em contato conosco!")

            navigate(
                user?.role === "admin" ? "/admin/musicas" : "/"
            )

        } catch (error) {

            showErrorToast("Falha ao compartilhar música!", error?.response?.data)
        }
    }

    return (
        <Form onSubmit={handleSubmit} className="w-50 d-flex flex-column gap-4 w-50 align-items-center">
            <Form.Group className="w-100" controlId="formNomeMusica">
                <Form.Label>Nome da Música</Form.Label>
                <Form.Control
                    type="text"
                    autoFocus
                    autoComplete="off"
                    placeholder="Digite o nome da música"
                    value={nomeMusica}
                    onChange={(e) => setNomeMusica(e.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group className="w-100" controlId="formAudio">
                <Form.Label className={errors?.errorAudio && "text-danger"}>Áudio</Form.Label>
                <Form.Control
                    className={errors?.errorAudio && "border-danger"}
                    type="file"
                    accept="audio/*"
                    onChange={(e) => setAudioFile(e.target.files[0])}
                    required
                />
                {errors?.errorAudio ?
                    <small className="form-text text-danger">{errors?.errorAudio?.message}</small>
                    :
                    <small className="form-text text-muted">Envie o áudio da música que você deseja compartilhar.</small>
                }
            </Form.Group>

            <Form.Group className="w-100" controlId="formArtista">
                <Form.Label className={errors?.errorArtista && "text-danger"}>Artista</Form.Label>
                <ArtistaCreatableSelect
                    className={errors?.errorArtista && "border-danger"}
                    options={options}
                    setOptions={setOptions}
                    createOption={createOption}
                    artista={artista}
                    setArtista={setArtista}
                />
                {errors?.errorArtista ?
                    <small className="form-text text-danger">{errors?.errorArtista?.message}</small>
                    :
                    <small className="form-text text-muted">Selecione um artista ou adicione um novo.</small>
                }
            </Form.Group>

            <Form.Group className="w-100" controlId="formTexto">
                <Form.Label>Texto (opcional)</Form.Label>
                <HtmlEditor texto={texto} setTexto={setTexto} />
                <small className="form-text text-muted">Deixe uma mensagem sobre a sua música para as pessoas!</small>
            </Form.Group>

            <Form.Group className="w-100" controlId="formImagem">
                <Form.Label className={errors?.errorImagem && "text-danger"}>Foto (opcional)</Form.Label>
                <Form.Control
                    className={errors?.errorImagem && "border-danger"}
                    type="file"
                    accept="image/*"
                    onChange={(e) => setImagem(e.target.files[0])}
                />
                {errors?.errorImagem ?
                    <small className="form-text text-danger">{errors?.errorImagem.message}</small>
                    :
                    <small className="form-text text-muted">Imagem da música que você deseja compartilhar.</small>
                }
            </Form.Group>

            <Button variant="primary" className="mt-4 py-3 fs-6 w-100" type="submit">
                Compartilhar Música!
            </Button>
        </Form>
    )
}

export default CreateMusica
