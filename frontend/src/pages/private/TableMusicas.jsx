import { useState, useEffect } from "react"
import { Button, Table } from "react-bootstrap"
import { fetchMusicas, deleteMusica } from "@/api/musica/musica"
import { AiFillDelete, AiFillEdit } from "react-icons/ai"
import { useErrorToast } from "@/hooks/useToast"

function TableMusicas() {
    const [musicas, setMusicas] = useState([])
    const showErrorToast = useErrorToast()

    useEffect(() => {
        const findAllMusicas = async () => {
            const musicas = await fetchMusicas()
            setMusicas(musicas)
        }
        findAllMusicas()
    }, [])

    const handleDeleteClick = async (musica) => {
        const deletedMusica = await deleteMusica({ id: musica._id })
        if (!deletedMusica) return showErrorToast("Não foi possível deletar a música!", "Houve algum imprevisto...")

        setMusicas((prevMusicas) => prevMusicas.filter(item => item._id !== musica._id));
    }

    return (
        <div className="d-flex flex-column gap-4 w-100" >
            <h1 className="text-primary text-center">Músicas</h1>
            <Table striped bordered hover responsive>
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Título</th>
                        <th>Texto</th>
                        <th>Artista</th>
                        <th>Usuário</th>
                        <th>Áudio</th>
                        <th>Imagem</th>
                        <th className="text-center">Ações</th>
                    </tr>
                </thead>
                <tbody>
                    {musicas.map((musica) => (
                        <tr key={musica._id}>

                            <td>{musica._id}</td>
                            <td>{musica.titulo}</td>
                            <td>{musica.texto || "Sem texto"}</td>
                            <td>{musica.artista}</td>
                            <td>{musica.username}</td>
                            <td>
                                <audio className="d-block mx-auto" controls src={musica.audio}>
                                    Your browser does not support the audio tag.
                                </audio>
                            </td>
                            <td >
                                <img width={"60x"} height={"60x"} src={musica.imagem || "/music.png"} className="rounded d-block mx-auto" alt={musica.titulo} />
                            </td>
                            <td className="p-3">
                                <div className="d-flex justify-content-center gap-2">
                                    <Button
                                        href={`/admin/editar/musica/${musica._id}`}
                                        variant="primary"
                                    >
                                        <AiFillEdit />
                                    </Button>{" "}
                                    <Button
                                        variant="danger"
                                        onClick={() => handleDeleteClick(musica)}
                                    >
                                        <AiFillDelete />
                                    </Button>
                                </div>
                            </td>
                        </tr>
                    ))}
                </tbody>
            </Table>
            <Button style={{ width: "200px", padding: "10px" }} variant="primary" href="/user/cadastro/musica">
                + Criar Nova Música
            </Button>
        </div >
    )
}

export default TableMusicas