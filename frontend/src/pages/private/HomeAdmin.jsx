import WelcomeCard from "@/components/private/WelcomeCard"
import { useSession } from "@/hooks/useSession"
import HomeCard from "@/components/private/HomeCard"
import { Row, Col, Button } from "react-bootstrap"
import {
    AiFillFile,
} from "react-icons/ai"
import {
    BsMusicPlayerFill,
    BsFillFileMusicFill
} from "react-icons/bs"
import {
    PiMicrophoneStageFill
} from "react-icons/pi"

export default function HomeAdmin() {

    const user = useSession()

    return (
        <>
            <WelcomeCard username={user?.username} />

            <Row className="w-100 gap-4">
                <Col className="d-flex justify-content-center">
                    <HomeCard
                        title={"Criar Música"}
                        icon={<BsFillFileMusicFill />}
                        body={"Compartilhe músicas para fortalecer nossa comunidade!"}
                        button={<Button href="/user/cadastro/musica">Criar agora!</Button>}
                    />
                </Col>
                <Col className="d-flex justify-content-center">
                    <HomeCard
                        icon={<BsMusicPlayerFill />}
                        title={"Gerenciar Músicas"}
                        body={"Visualize e gerencie nossas músicas para garantir a segurança da nossa comunidade!"}
                        button={<Button href="/admin/musicas">Gerencie agora!</Button>}
                    />
                </Col>
                <Col className="d-flex justify-content-center">
                    <HomeCard
                        icon={<PiMicrophoneStageFill />}
                        title={"Gerenciar Artistas"}
                        body={"Veja quais artistas temos na nossa comunidade!"}
                        button={<Button href="/admin/artistas">Visualize agora!</Button>}
                    />
                </Col>

            </Row>
        </>
    )
}
