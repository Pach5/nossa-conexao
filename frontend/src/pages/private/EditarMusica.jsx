import { useEffect, useRef, useState } from "react"
import { Form, Button } from "react-bootstrap"
import { useParams, useNavigate } from "react-router-dom"
import { useErrorToast } from "@/hooks/useToast"
import ArtistaCreatableSelect from "@/components/ui/ArtistaCreatableSelect"
import {
    AiFillCloseCircle
} from "react-icons/ai"
import {
    fetchArtistas,
    fetchMusicaById,
    saveImagem,
    saveAudio,
    saveArtista,
    updateMusica
} from "@/api/musica/musica"
import { useSession } from "@/hooks/useSession"

function EditarMusica() {
    const { id } = useParams()
    const navigate = useNavigate()

    const user = useSession()
    const showErrorToast = useErrorToast()

    const fotoRef = useRef()
    const audioRef = useRef()

    const [nomeMusica, setNomeMusica] = useState("")
    const [texto, setTexto] = useState("")
    const [artista, setArtista] = useState({})

    const [existingFoto, setExistingFoto] = useState("")
    const [existingAudioUrl, setExistingAudioUrl] = useState("")
    const [imagem, setImagem] = useState(null)
    const [audioFile, setAudioFile] = useState(null)

    const [existingMusicData, setExistingMusicData] = useState({})

    const [errors, setErrors] = useState([])
    const [options, setOptions] = useState([])

    const createOption = (label, value) => ({
        label,
        value
    })

    useEffect(() => {
        const setOptionsSelectArtista = async () => {
            const artistas = await fetchArtistas()
            const options = artistas.map((option) =>
                createOption(option.nome, option._id)
            )
            setOptions(options)
        }

        setOptionsSelectArtista()

        const fetchMusicaData = async () => {
            try {
                const musicaData = await fetchMusicaById({ id })
                setNomeMusica(musicaData.titulo)
                setTexto(musicaData.texto)
                setArtista(createOption(musicaData.artista, musicaData.artistaId))
                setExistingAudioUrl(musicaData.audio)
                setExistingMusicData(musicaData)
                musicaData.imagem && setExistingFoto(musicaData.imagem)
            } catch (error) {
                showErrorToast("Erro ao buscar música!", "Não foi possível coletar os dados da música requisitada...")
            }
        }

        fetchMusicaData()
    }, [id])

    const isFormValid = () => {
        setErrors([])

        let isValid = true
        const MAX_FILE_SIZE_40_MB = 40 * 1024 * 1024
        const FILE_ERROR_MESSAGE = "Arquivo muito grande! Aceitamos no máximo 40MB!"

        if (audioFile && audioFile.size > MAX_FILE_SIZE_40_MB) {
            setErrors(prevErrors => ({ ...prevErrors, errorAudio: { message: FILE_ERROR_MESSAGE } }))
            isValid = false
        }

        if (imagem && imagem.size > MAX_FILE_SIZE_40_MB) {
            setErrors(prevErrors => ({ ...prevErrors, errorImagem: { message: FILE_ERROR_MESSAGE } }))
            isValid = false
        }

        if (!artista || Object.keys(artista).length == 0) {
            setErrors(prevErrors => ({ ...prevErrors, errorArtista: { message: "Por favor, selecione um artista!" } }))
            isValid = false
        }
        return isValid
    }

    const isEdicaoVazia = (edicaoData, originalData) => {
        return (
            (edicaoData.titulo === originalData.titulo || !edicaoData.titulo) &&
            (edicaoData.texto === originalData.texto || !edicaoData.texto) &&
            (edicaoData.artistaId === originalData.artistaId || !edicaoData.artistaId) &&
            edicaoData.imagem === null &&
            edicaoData.audio === null
        );
    };

    const handleSubmit = async (e) => {
        e.preventDefault();
        if (!isFormValid()) return;

        try {
            let artistaId = null;
            if (!artista?.value) {
                const novoArtista = await saveArtista({ nome: artista?.label });
                artistaId = novoArtista._id;
            } else {
                artistaId = artista?.value;
            }

            let fotoId = existingMusicData.fotoId
            if (imagem) {
                const formDataImagem = new FormData();
                formDataImagem.append("imagem", imagem);
                const savedImagem = await saveImagem(formDataImagem);
                fotoId = savedImagem._id;
            }

            let arquivoId = existingMusicData.arquivoId
            if (audioFile) {
                const formDataAudio = new FormData();
                formDataAudio.append("audio", audioFile);
                const savedAudio = await saveAudio(formDataAudio);
                arquivoId = savedAudio?._id;
            }

            const updatedMusicaData = {
                id: +id,
                titulo: nomeMusica,
                texto: texto === "" ? existingMusicData.texto : texto,
                artistaId,
                arquivoId,
                fotoId,
                usuarioId: user?._id
            };

            // Update only if the data has changed
            if (!isEdicaoVazia(updatedMusicaData, existingMusicData)) {
                // Use the function to update the musica object
                const updatedMusica = await updateMusica(updatedMusicaData)
                console.log(updatedMusica)
                if (!updatedMusica) {
                    return showErrorToast(
                        "Falha ao atualizar música!",
                        "Houve algum erro imprevisto. Por favor, tente novamente mais tarde ou entre em contato conosco!"
                    );
                }
            }

            navigate(user?.role === "admin" ? "/admin/musicas" : "/");
        } catch (error) {
            showErrorToast("Falha ao atualizar música!", error?.response?.data);
        }
    }

    return (
        <Form onSubmit={handleSubmit} className="w-50 d-flex flex-column gap-4 w-50 align-items-center">
            <Form.Group className="w-100" controlId="formNomeMusica">
                <Form.Label>Nome da Música</Form.Label>
                <Form.Control
                    type="text"
                    autoFocus
                    autoComplete="off"
                    placeholder="Digite o nome da música"
                    value={nomeMusica}
                    onChange={(e) => setNomeMusica(e.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group className="w-100" controlId="formAudio">
                <Form.Label className={errors?.errorAudio && "text-danger"}>Áudio</Form.Label>
                <Form.Control
                    className={errors?.errorAudio && "border-danger"}
                    type="file"
                    ref={audioRef}
                    accept="audio/*"
                    onChange={(e) => setAudioFile(e.target.files[0])}
                />

                {errors?.errorAudio ?
                    <small className="form-text text-danger">{errors?.errorAudio?.message}</small>
                    :
                    <small className="form-text text-muted">Envie o áudio da música que você deseja compartilhar.</small>
                }
                {existingAudioUrl && (
                    <div className="w-100 d-flex justify-content-center gap-5 mt-4 flex-wrap">
                        <div className="d-flex w-100 flex-wrap gap-2 justify-content-between align-items-center mt-3">
                            <span style={{ width: "20%" }} className="text-primary">Faixa atual</span>
                            <audio style={{ width: "70%" }} controls>
                                <source src={existingAudioUrl} type="audio/mpeg" />
                                Your browser does not support the audio element.
                            </audio>
                        </div>
                        {audioFile && (
                            <div className="d-flex w-100 flex-wrap gap-2 justify-content-between align-items-center mt-3">
                                <span style={{ width: "20%" }} className="text-primary">Faixa atualizada</span>
                                <audio style={{ width: "70%" }} controls>
                                    <source src={URL.createObjectURL(audioFile)} type="audio/mpeg" />
                                    Your browser does not support the audio element.
                                </audio>
                                <AiFillCloseCircle
                                    onClick={() => {
                                        setAudioFile(null)
                                        audioRef.current.value = ""
                                    }}
                                    style={{ cursor: "pointer", height: "100%", fontSize: "1.5rem" }} />
                            </div>
                        )}

                    </div>
                )}
            </Form.Group>

            <Form.Group className="w-100" controlId="formArtista">
                <Form.Label className={errors?.errorArtista && "text-danger"}>Artista</Form.Label>
                <ArtistaCreatableSelect
                    className={errors?.errorArtista && "border-danger"}
                    options={options}
                    setOptions={setOptions}
                    createOption={createOption}
                    artista={artista}
                    setArtista={setArtista}
                />
                {errors?.errorArtista ?
                    <small className="form-text text-danger">{errors?.errorArtista?.message}</small>
                    :
                    <small className="form-text text-muted">Selecione um artista ou adicione um novo.</small>
                }
            </Form.Group>

            <Form.Group className="w-100" controlId="formTexto">
                <Form.Label>Texto (opcional)</Form.Label>
                <Form.Control
                    autoComplete="off"
                    as="textarea" rows={3}
                    value={texto}
                    onChange={(e) => setTexto(e.target.value)}
                />
                <small className="form-text text-muted">Deixe uma mensagem sobre a sua música para as pessoas!</small>
            </Form.Group>

            <Form.Group className="w-100" controlId="formImagem">
                <Form.Label className={errors?.errorImagem && "text-danger"}>Foto (opcional)</Form.Label>
                <Form.Control
                    className={errors?.errorImagem && "border-danger"}
                    type="file"
                    ref={fotoRef}
                    accept="image/*"
                    onChange={(e) => setImagem(e.target.files[0])}
                />
                {errors?.errorImagem ?
                    <small className="form-text text-danger">{errors?.errorImagem.message}</small>
                    :
                    <small className="form-text text-muted">Imagem da música que você deseja compartilhar.</small>
                }
                {existingFoto !== "" && (
                    <div className="w-100 d-flex justify-content-center gap-5 mt-4 flex-wrap">
                        <div className="d-flex flex-column gap-2">
                            <span className="text-primary">Foto atual</span>
                            <img width={"200px"} height={"200px"} src={existingFoto} className="rounded" alt={nomeMusica} />
                        </div>
                        {imagem && (
                            <div className="d-flex flex-column gap-2">
                                <div className="w-100 d-flex justify-content-between">
                                    <span className="text-primary">Foto atualizada</span>
                                    <AiFillCloseCircle
                                        onClick={() => {
                                            setImagem(null)
                                            fotoRef.current.value = ""
                                        }}
                                        style={{ cursor: "pointer", height: "100%", fontSize: "1.5rem" }} />
                                </div>
                                <img width={"200px"} height={"200px"} src={URL.createObjectURL(imagem)} className="rounded" alt={nomeMusica} />
                            </div>
                        )}
                    </div>
                )}
            </Form.Group>

            <Button variant="primary" className="mt-4 py-3 fs-6 w-100" type="submit">
                Atualizar Música!
            </Button>
        </Form>
    )
}

export default EditarMusica
