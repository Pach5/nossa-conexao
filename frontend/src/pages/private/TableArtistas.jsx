import { useState, useEffect } from "react"
import { Table, Form, Button } from "react-bootstrap"
import { fetchArtistas, saveArtista } from "@/api/musica/musica"
import { useErrorToast } from "@/hooks/useToast"

function TableArtistas() {
    const [artistas, setArtistas] = useState([])
    const showErrorToast = useErrorToast()

    useEffect(() => {
        const findAllArtistas = async () => {
            const artistas = await fetchArtistas()
            setArtistas(artistas)
        }
        findAllArtistas()
    }, [])

    const [novoArtista, setNovoArtista] = useState("")

    const handleNovoArtista = async (e) => {
        e.preventDefault()

        if (artistas.
            find((artista) =>
                artista.nome.toLowerCase() === novoArtista.toLocaleLowerCase()
            )
        ) {
            return showErrorToast("Artista já existe!", "Operação cancelada...")
        }

        const savedArtista = await saveArtista({ nome: novoArtista })

        setArtistas([...artistas, savedArtista])
        setNovoArtista("")
    }

    return (
        <div className="d-flex flex-column gap-4 w-100">
            <h1 className="text-primary text-center">Artistas</h1>
            <Table striped bordered hover>
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Nome</th>
                    </tr>
                </thead>
                <tbody>
                    {artistas.map(artista => (
                        <tr key={artista._id}>
                            <td>{artista._id}</td>
                            <td>{artista.nome}</td>
                        </tr>
                    ))}
                </tbody>
            </Table>
            <Form onSubmit={handleNovoArtista} className="d-flex gap-3 w-25">
                <Form.Group>
                    <Form.Control
                        type="text"
                        placeholder="+ Criar Novo Artista"
                        value={novoArtista}
                        onChange={(e) => setNovoArtista(e.target.value)}
                    />
                </Form.Group>
                <Button variant="primary" type="submit">
                    +
                </Button>
            </Form>

        </div >
    )
}

export default TableArtistas
