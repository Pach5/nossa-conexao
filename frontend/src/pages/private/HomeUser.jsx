import WelcomeCard from "@/components/private/WelcomeCard"
import { useSession } from "@/hooks/useSession"
import HomeCard from "@/components/private/HomeCard"
import { Row, Col, Button } from "react-bootstrap"
import {
    BsFillFileMusicFill
} from "react-icons/bs"

export default function HomeAdmin() {

    const user = useSession()

    return (
        <>
            <WelcomeCard username={user?.username} />

            <Row className="w-100 gap-4">
                <Col className="d-flex justify-content-center">
                    <HomeCard
                        title={"Criar Música"}
                        icon={<BsFillFileMusicFill />}
                        body={"Compartilhe músicas para fortalecer nossa comunidade!"}
                        button={<Button href="/user/cadastro/musica">Criar agora!</Button>}
                    />
                </Col>
            </Row>
        </>
    )
}
