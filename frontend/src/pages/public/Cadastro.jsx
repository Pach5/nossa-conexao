import React, { useState } from "react"
import { Container, Form, Button } from "react-bootstrap"
import { signUp } from "@/api/usuario/usuario.js"
import { useErrorToast } from "@/hooks/useToast"
import { useNavigate } from "react-router-dom"

export default function SignupPage() {
    const navigate = useNavigate()
    const showErrorToast = useErrorToast()

    const [username, setUsername] = useState("")
    const [senha, setSenha] = useState("")

    const handleUsernameChange = (event) => {
        setUsername(event.target.value)
    }

    const handleSenhaChange = (event) => {
        setSenha(event.target.value)
    }

    const handleSubmit = async (event) => {
        event.preventDefault()

        try {

            const usuario = await signUp({
                username, senha
            })

            if (!usuario?._id) return showErrorToast("Falha ao cadastrar usuário!", "Não foi possível registrar o usuário.")

            sessionStorage.setItem("token", JSON.stringify(usuario))
            return navigate(`/${usuario.role}`)
        } catch (e) {
            showErrorToast("Falha ao cadastrar!", e?.response?.data)
        }
    }

    return (
        <Container className="mt-5 d-flex flex-column align-items-center">
            <h1>Cadastro de Usuário</h1>
            <Form onSubmit={handleSubmit} className="p-3 h-100 w-50">
                <Form.Group className="mb-3" controlId="username">
                    <Form.Label>Username</Form.Label>
                    <Form.Control
                        autoComplete="off"
                        type="text"
                        autoFocus
                        placeholder="Digite seu username"
                        value={username}
                        onChange={handleUsernameChange}
                    />
                </Form.Group>

                <Form.Group className="mb-3" controlId="senha">
                    <Form.Label>Senha</Form.Label>
                    <Form.Control
                        autoComplete="off"
                        type="password"
                        placeholder="Digite sua senha"
                        value={senha}
                        onChange={handleSenhaChange}
                    />
                </Form.Group>

                <Button className="mt-3 py-2 fs-5 w-100" variant="primary" type="submit">
                    Cadastrar
                </Button>
            </Form>
        </Container>
    )
}
