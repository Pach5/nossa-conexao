import { useState } from "react"
import { Container, Form, Button } from "react-bootstrap"
import { useNavigate } from "react-router-dom"
import { useErrorToast } from "@/hooks/useToast"
import { signIn } from "@/api/usuario/usuario.js"

export default function Login() {
  const navigate = useNavigate()
  const showErrorToast = useErrorToast();

  const [username, setUsername] = useState("")
  const [senha, setSenha] = useState("")

  const handleUsernameChange = (event) => {
    setUsername(event.target.value)
  }

  const handleSenhaChange = (event) => {
    setSenha(event.target.value)
  }

  const handleSubmit = async (event) => {
    event.preventDefault()
    try {
      const token = await signIn({ username, senha })

      if (!token) return showErrorToast("Falha ao logar!", "Username e/ou senha inválido(s)!")

      sessionStorage.setItem("token", JSON.stringify(token))
      return navigate(`/${token.role}`)

    } catch (e) {
      showErrorToast("Falha ao logar!", e?.response?.data)
    }

  }

  return (
    <Container className="mt-5 d-flex flex-column align-items-center">
      <h1>Login</h1>
      <Form onSubmit={handleSubmit} className="p-3 h-100 w-50">
        <Form.Group className="mb-3" controlId="username">
          <Form.Label>Username</Form.Label>
          <Form.Control
            autoFocus
            type="text"
            placeholder="Digite seu username"
            value={username}
            autoComplete="off"
            onChange={handleUsernameChange}
          />
        </Form.Group>

        <Form.Group className="mb-3" controlId="Senha">
          <Form.Label>Senha</Form.Label>
          <Form.Control
            type="password"
            placeholder="Digite sua senha"
            value={senha}
            autoComplete="off"
            onChange={handleSenhaChange}
          />
        </Form.Group>

        <Button className="mt-3 py-2 fs-5 w-100" variant="primary" type="submit">
          Entrar
        </Button>
      </Form>
    </Container>
  )
}