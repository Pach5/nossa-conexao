import MusicaCard from "@/components/ui/MusicaCard"
import {
    BsFillPlayFill,
    BsPauseFill
} from "react-icons/bs"
import {
    AiFillCloseCircle
} from "react-icons/ai"
import { useState, useEffect } from 'react'
import { fetchMusicas } from "@/api/musica/musica"
import { Container, Button } from "react-bootstrap"

export default function FeedMusicas() {

    useEffect(() => {
        const findAllMusicas = async () => {
            const musicas = await fetchMusicas()
            setMusicas(musicas)
        }
        findAllMusicas()
    }, [])

    const [isPlaying, setIsPlaying] = useState(false)
    const [displayControls, setDisplayControls] = useState("none")
    const [playingMusica, setPlayingMusica] = useState({})
    const [playingAudio, setPlayingAudio] = useState()
    const [musicas, setMusicas] = useState([])

    const handleOpenControls = () => {
        setDisplayControls("flex")
    }

    const handleCloseControls = () => {
        setDisplayControls("none")
        setIsPlaying(false)
        setPlayingMusica({})
        setPlayingAudio(null)
    }

    const handleMusica = (musica, audio) => {
        !isPlaying && handleOpenControls()

        if (isPlaying && audio == playingAudio) {
            setIsPlaying(false)
            playingAudio.pause()
            return
        }

        if (isPlaying && audio !== playingAudio) {
            setIsPlaying(false)
            playingAudio.pause()
        }

        setPlayingAudio(audio)
        setPlayingMusica(musica)
        setIsPlaying(true)
        audio.currentTime = 0
        audio.play()
    }

    return (
        <>
            <Container className="d-flex align-items-center flex-column gap-5 text-left">
                <h2 className="text-primary w-100">Feed de Músicas</h2>
                {
                    musicas.length > 0 &&
                    musicas.map((musica) => (
                        <MusicaCard
                            key={musica._id}
                            isPlaying={isPlaying}
                            playingAudio={playingAudio}
                            setIsPlaying={setIsPlaying}
                            handleMusica={handleMusica}
                            musica={musica}
                        />
                    ))
                }
                <Button variant="secondary" className="w-100 py-3 mt-3" href="/user/cadastro/musica">
                    Compartilhe músicas agora para aparecer no feed!
                </Button>
            </Container >

            <div style={{
                display: displayControls,
                backgroundColor: "#eee",
                zIndex: 1000
            }}
                className="position-fixed shadow-lg flex-column justify-content-around p-3 bottom-0 w-100 h-25">
                <div className="d-flex justify-content-between">
                    <h6>Você está ouvindo...</h6>
                    <AiFillCloseCircle
                        onClick={() => {
                            handleCloseControls()
                            playingAudio.pause()
                        }}
                        style={{ cursor: "pointer", height: "100%", fontSize: "1.5rem" }} />
                </div>
                <div className="d-flex gap-3">
                    <Button
                        style={{ width: "90px", fontSize: "3rem" }}
                        onClick={() => {
                            setIsPlaying(!isPlaying)
                            isPlaying ? playingAudio.pause() : playingAudio.play()
                        }}>
                        {isPlaying ?
                            <BsPauseFill />
                            :
                            <BsFillPlayFill />
                        }
                    </Button>
                    <img width={"100px"} height={"100px"} src={playingMusica.imagem || "/music.png"} className="rounded d-block" alt={playingMusica.titulo} />
                    <div>
                        <h5>{playingMusica.titulo}</h5>
                        <h6>{playingMusica.artista}</h6>

                        <p>Recomendado por {" "}
                            <span className="text-primary">
                                {playingMusica.username === "admin" ? "Nossa Conexão" : playingMusica.username}
                            </span>
                        </p>
                    </div>
                </div>
            </div >
        </>
    )
};
