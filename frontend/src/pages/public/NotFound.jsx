import { Button, Container } from "react-bootstrap"

export default function NotFound() {
  return (
    <Container className="d-flex align-items-center flex-column gap-3 mt-5">
      <h1>Página não encontrada!</h1>
      <Button href="/" variant="primary">Voltar à página inicial</Button>
    </Container>

  );
}
