import { Container, Button } from "react-bootstrap"
import FeedMusicas from "@/pages/public/FeedMusicas"

export default function Homepage() {
  return (
    <div className="d-flex flex-column gap-5 mt-5">
      <div className="mx-5">
        <hr />
      </div>
      <Container>
        <h1 className="text-primary">Bem-vindo ao nosso site de compartilhamento de músicas!</h1>
        <h3 className="text-secondary">Descubra e compartilhe suas músicas favoritas.</h3>
        <Button
          style={{ width: "250px" }}
          className="py-3 mt-3"
          href="/user/cadastro/musica"
          variant="primary"
        >
          Comece agora!
        </Button>
      </Container>
      <div className="mx-5">
        <hr />
      </div>
      <FeedMusicas />
    </div >

  );
}
