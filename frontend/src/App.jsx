import { Routes } from "react-router-dom"
import PrivateRoutes from "@/routes/PrivateRoutes"
import PublicRoutes from "@/routes/PublicRoutes"
import { BrowserRouter as Router } from "react-router-dom"
import Providers from "./Providers"

import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css"

function App() {
  return (
    <Router>
      <Providers>
        <Routes>
          {PrivateRoutes}
          {PublicRoutes}
        </Routes>
      </Providers>
    </Router>
  )
}

export default App
