import { ToastProvider } from "@/context/ToastContext"

export default function Providers({ children }) {
    return (
        <ToastProvider>
            {children}
        </ToastProvider>
    )
};
