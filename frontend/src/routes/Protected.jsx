import { useLocation, Navigate } from "react-router-dom"

/*
 * Snippet para liberar acesso a todas as páginas
 * Desativa Role-Based Access
 */
// const mode = import.meta.env.VITE_MODE
// if (mode === "test") return children

export default function Protected({ children }) {
  const redirect = { to: null }
  const location = useLocation()

  const token = sessionStorage.getItem("token")
  if (!token) {
    redirect.to = "/login"
  } else {
    const { role } = JSON.parse(token)
    if (location.pathname.startsWith("/admin") && role === "user") {
      redirect.to = "/user"
    }
  }

  return redirect.to ? <Navigate to={redirect.to} replace /> : <>{children}</>
}
