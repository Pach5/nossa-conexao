import Protected from "@/routes/Protected"
import { Route } from "react-router-dom"
import TableMusicas from "@/pages/private/TableMusicas"
import TableArtistas from "@/pages/private/TableArtistas"
import EditarMusica from "@/pages/private/EditarMusica"
import CreateMusica from "@/pages/private/CreateMusica"
import HomeAdmin from "@/pages/private/HomeAdmin"
import HomeUser from "@/pages/private/HomeUser"
import PrivateLayout from "@/layout/PrivateLayout"

export default [
  <Route element={
    <Protected>
      <PrivateLayout />
    </Protected>
  } key={"dashboard"}>

    <Route exact path="/admin" element={<HomeAdmin />} />
    <Route exact path="/admin/musicas" element={<TableMusicas />} />
    <Route exact path="/admin/artistas" element={<TableArtistas />} />
    <Route exact path="/admin/editar/musica/:id" element={<EditarMusica />} />

    <Route exact path="/user" element={<HomeUser />} />
    <Route exact path="/user/cadastro/musica" element={<CreateMusica />} />

  </Route>
]
