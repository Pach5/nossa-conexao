import { Route } from "react-router-dom"

import PublicLayout from "@/layout/PublicLayout"

import Homepage from "@/pages/public/Homepage"
import Cadastro from "@/pages/public/Cadastro"
import NotFound from "@/pages/public/NotFound"
import Login from "@/pages/public/Login"

export default [

  <Route element={<PublicLayout />} key={"client"}>

    <Route exact path="/" element={<Homepage />} />
    <Route exact path="/cadastro" element={<Cadastro />} />
    <Route exact path="/login" element={<Login />} />
    <Route exact path="*" element={<NotFound />} />

  </Route>,

]
