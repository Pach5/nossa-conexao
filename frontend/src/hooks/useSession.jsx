import { useState } from "react"

export const useSession = () => {
    const [user, setUser] = useState(() => {
        try {
            const token = sessionStorage.getItem("token")

            if (token) {
                return JSON.parse(token)
            }

        } catch (e) {
            return null
        }
    })

    return user
}