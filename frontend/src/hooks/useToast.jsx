import { useToast } from "@/context/ToastContext";

export function useErrorToast() {
    const { showToast } = useToast();

    return (title, message) => showToast("danger", title, message);
}

export function useSuccessToast() {
    const { showToast } = useToast();

    return (title, message) => showToast("success", title, message);
}

export function useWarningToast() {
    const { showToast } = useToast();

    return (title, message) => showToast("warning", title, message);
}
