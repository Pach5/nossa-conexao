-- Criar o banco de dados
DROP DATABASE IF EXISTS nossa_conexao;
CREATE DATABASE IF NOT EXISTS nossa_conexao;

-- Usar o banco de dados
USE nossa_conexao;

-- Tabela para artistas
CREATE TABLE IF NOT EXISTS artista (
    id INT AUTO_INCREMENT PRIMARY KEY,
    nome VARCHAR(255) UNIQUE NOT NULL
);

-- Tabela para arquivos
CREATE TABLE IF NOT EXISTS arquivo (
    id INT AUTO_INCREMENT PRIMARY KEY,
    nome VARCHAR(200) UNIQUE NOT NULL,
    tipo VARCHAR(50) NOT NULL,
    tamanho INT NOT NULL,
    path VARCHAR(255) UNIQUE NOT NULL
);

-- Tabela de usuários
CREATE TABLE IF NOT EXISTS usuario (
    id INT AUTO_INCREMENT PRIMARY KEY,
    username TEXT UNIQUE NOT NULL,
    senha VARCHAR(255) NOT NULL,
    role VARCHAR(20) NOT NULL,
    salt VARCHAR(255) NOT NULL,
    ativo TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

-- Tabela para músicas
CREATE TABLE IF NOT EXISTS musica (
    id INT AUTO_INCREMENT PRIMARY KEY,
    titulo VARCHAR(200) NOT NULL,
    texto TEXT,
    artista_id INT,
    arquivo_id INT,
    usuario_id INT,
    foto_id INT,
    FOREIGN KEY (artista_id) REFERENCES artista(id),
    FOREIGN KEY (arquivo_id) REFERENCES arquivo(id) ON DELETE CASCADE,
    FOREIGN KEY (usuario_id) REFERENCES usuario(id),
    FOREIGN KEY (foto_id) REFERENCES arquivo(id) ON DELETE CASCADE
);

