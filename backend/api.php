<?php

include __DIR__ . '/autoload.php';
include __DIR__ . '/confs/config.php';

error_reporting(E_ERROR | E_WARNING | E_PARSE | E_NOTICE);

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: Content-Type');

ini_set('default_charset', 'utf-8');
setlocale(LC_ALL, 'pt_BR.UTF-8', 'Portuguese_Brazil.1252');

$handler = $_REQUEST["controller"] ?? "";
$action = $_REQUEST["action"] ?? "";

$controller = new $handler();

if (method_exists($controller::class, $action)) {
    $controller->{$action}();
} else {
    $controller->notFoundResponse();
}
