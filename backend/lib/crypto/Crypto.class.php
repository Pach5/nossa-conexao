<?php

class Crypto
{
    public static function generateSalt()
    {
        return bin2hex(random_bytes(16));
    }

    public static function hashPassword($password, $salt)
    {
        return hash("sha256", $password . $salt);
    }
}
