<?php

class AuthManager
{

    private static function setArrayAuthedUser($user)
    {
        $authedUser = new Usuario();
        $authedUser->setId($user->getId());
        $authedUser->__set("username", $user->__get("username"));
        $authedUser->__set("role", $user->__get("role"));
        $authedUser->__set("ativo", $user->__get("ativo"));
        return $authedUser;
    }

    public static function authenticate(Usuario $user, string $providedPassword)
    {
        $hashedPassword = Crypto::hashPassword($providedPassword, $user->__get("salt"));

        $authedUser = null;
        if ($isAuthenticated = $hashedPassword === $user->__get("senha")) {
            session_start();
            $authedUser = self::setArrayAuthedUser($user);
            $_SESSION["token"] = $authedUser;
        }
        return $isAuthenticated ? $authedUser : false;
    }

    public static function logout()
    {
        session_start();

        if (isset($_SESSION['token'])) {
            session_unset();
            session_destroy();
            return true;
        }

        return false;
    }
}
