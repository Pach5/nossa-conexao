<?php

use Firebase\JWT\JWT;

abstract class AbstractController
{

    // REQUEST TREATMENT

    public function response($response, $statusCode = 200)
    {
        http_response_code($statusCode);
        header('Content-Type: application/json; charset=utf-8');
        echo json_encode($response);
    }

    public function badRequestResponse($message = "Bad request.")
    {
        $this->response($message, 400);
    }

    public function unauthorizedResponse($message = "Unauthorized.")
    {
        $this->response($message, 401);
    }

    public function forbiddenResponse($message = "Forbidden.")
    {
        $this->response($message, 403);
    }

    public function notFoundResponse($message = "Not found.")
    {
        $this->response($message, 404);
    }

    public function methodNotAllowedResponse($message = "Method not allowed.")
    {
        $this->response($message, 405);
    }

    public function unsupportedMediaTypeResponse($message = "Unsupported Media Type.")
    {
        $this->response($message, 415);
    }

    public function getRequestBody()
    {
        $request_body = file_get_contents("php://input");
        $data = json_decode($request_body, true);
        return $data;
    }

    public function redirectHome()
    {
        header("Location: /");
    }

    // CRUD 

    protected function create($model, $data, $additionalData = [])
    {
        if ($_SERVER["REQUEST_METHOD"] !== "POST") {
            return $this->methodNotAllowedResponse();
        }

        // if (!$this->verifySession()) {
        // return $this->unauthorizedResponse();
        // }

        $entity = new $model();
        foreach ($data as $key => $value) {
            if (property_exists($entity, $key)) {
                $entity->__set($key, $value);
            }
        }

        foreach ($additionalData as $key => $value) {
            if (property_exists($entity, $key)) {
                $entity->__set($key, $value);
            }
        }

        return $this->response($entity->save(), 201);
    }

    protected function update($model, $data)
    {
        if (!in_array($_SERVER["REQUEST_METHOD"], ["POST", "PUT"])) {
            return $this->methodNotAllowedResponse();
        }

        // if (!$this->verifySession()) {
        //     return $this->unauthorizedResponse();
        // }

        $entity = new $model();
        $entity->setId($data["id"]);
        $entity->lazyLoad();

        // Comparar os dados em $data com os dados da entidade $entity
        foreach ($data as $property => $value) {
            if (property_exists($entity, $property) && $entity->__get($property) !== $value) {
                $entity->__set($property, $value);
            }
        }
        return $this->response($entity->update());
    }

    protected function delete($model, $data, $permissionCheck = true)
    {
        if ($_SERVER["REQUEST_METHOD"] !== "DELETE") {
            return $this->methodNotAllowedResponse();
        }

        // $sessionRole = $this->verifySession();

        // if ($permissionCheck && !$sessionRole) {
        //     return $this->unauthorizedResponse();
        // }

        // if ($permissionCheck) {
        //     if ($sessionRole === "user") {
        //         return $this->forbiddenResponse();
        //     }
        // }

        $entity = new $model();
        $entity->setId($data["id"]);

        return $this->response($entity->delete());
    }

    protected function findOne($model, $data)
    {
        if ($_SERVER["REQUEST_METHOD"] !== "GET") {
            return $this->methodNotAllowedResponse();
        }

        // if (!$this->verifySession()) {
        //     return $this->unauthorizedResponse();
        // }

        $entity = new $model();
        $entity->setId($data["id"]);

        return $this->response($entity->lazyLoad());
    }

    protected function findAll($model)
    {
        if ($_SERVER["REQUEST_METHOD"] !== "GET") {
            return $this->methodNotAllowedResponse();
        }

        $entities = $model::findAll();
        return $this->response($entities);
    }
}
