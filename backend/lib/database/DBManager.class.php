<?php

class DBManager
{


    //=-=-=-==-=-=-==-=-=-==-=-=-==-=-=-==-=-=-==-=-=-==-=-=-==-=-=-==-=-=-==-=-=-==-=-=-==-
    //*=-=-=-=-=-=-=-=-=-=//=-=-=-=-=- Sessão -=-=-=-=-=//=-=-=-=-=-=-=-=-=-=-=-=//=-=-=-=-=
    //=-=-=-==-=-=-==-=-=-==-=-=-==-=-=-==-=-=-==-=-=-==-=-=-==-=-=-==-=-=-==-=-=-==-=-=-==-


    public static function connect()
    {
        $host = "localhost";
        $user = "root";
        $password = "";
        $db = "nossa_conexao";

        try {
            $dsn = "mysql:host=$host;port=3306;dbname=$db;";

            return new PDO(
                $dsn,
                $user,
                $password,
                [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]
            );
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }

    public static function disconnectPDO(PDO &$pdo)
    {
        $pdo = null;
    }

    public static function disconnect(PDO &$pdo, PDOStatement &$stmt)
    {
        $pdo = null;
        $stmt = null;
    }


    //=-=-=-==-=-=-==-=-=-==-=-=-==-=-=-==-=-=-==-=-=-==-=-=-==-=-=-==-=-=-==-=-=-==-=-=-==-
    //*=-=-=-=-=-=-=-=-=-=//=-=-=-=-=- DML -=-=-=-=-=//=-=-=-=-=-=-=-=-=-=-=-=//=-=-=-=-=-=-
    //=-=-=-==-=-=-==-=-=-==-=-=-==-=-=-==-=-=-==-=-=-==-=-=-==-=-=-==-=-=-==-=-=-==-=-=-==-


    /**
     * Salva o objeto no banco de dados.
     *
     * @return mixed O objeto salvo no banco ou false em caso de falha.
     */
    public static function save($object)
    {
        $pdo = self::connect();
        $stmt = null;

        try {
            $tableName = $object::getTableName();
            $attributes = $object->getPublicAttributes();

            ["sql" => $sql, "values" => $values] = SqlQueryBuilder::buildInsertQuery($tableName, $attributes);

            $stmt = $pdo->prepare($sql);
            $stmt->execute($values);

            if ($stmt->rowCount() > 0) {
                if (!($object::isJoinTable())) {
                    $object->setId(intval($pdo->lastInsertId()));
                }
                return $object;
            }
            return false;
        } catch (PDOException $e) {
            return $e->getMessage();
        } finally {
            self::disconnect($pdo, $stmt);
        }
    }

    /**
     * Atualiza o objeto no banco de dados.
     *
     * @return mixed O objeto atualizado no banco ou false em caso de erro.
     */
    public static function update($object)
    {
        $pdo = self::connect();
        $stmt = null;

        try {
            $tableName = $object::getTableName();
            $attributes = $object->getPublicAttributes();

            ["sql" => $sql, "params" => $params] = SqlQueryBuilder::buildUpdateQuery($tableName, $attributes, $object->getCondition());

            $stmt = $pdo->prepare($sql);
            $stmt->execute($params);

            if ($stmt->rowCount() > 0) {
                return $object;
            }
            return false;
        } catch (PDOException $e) {
            return $e->getMessage();
        } finally {
            self::disconnect($pdo, $stmt);
        }
    }

    /**
     * Exclui o objeto do banco de dados.
     *
     * @return mixed o objeto se for excluído com sucesso, false em caso de erro.
     */
    public static function delete($object)
    {
        $pdo = self::connect();
        $stmt = null;

        try {
            $tableName = $object::getTableName();

            $sql = "DELETE FROM $tableName WHERE " . $object->getCondition();
            $stmt = $pdo->prepare($sql);
            $stmt->execute();

            if ($stmt->rowCount() > 0) {
                return $object;
            }
            return false;
        } catch (PDOException $e) {
            return $e->getMessage();
        } finally {
            self::disconnect($pdo, $stmt);
        }
    }


    //=-=-=-==-=-=-==-=-=-==-=-=-==-=-=-==-=-=-==-=-=-==-=-=-==-=-=-==-=-=-==-=-=-==-=-=-==-
    //*=-=-=-=-=-=-=-=-=-=//=-=-=-=-=- DQL -=-=-=-=-=//=-=-=-=-=-=-=-=-=-=-=-=//=-=-=-=-=-=-
    //=-=-=-==-=-=-==-=-=-==-=-=-==-=-=-==-=-=-==-=-=-==-=-=-==-=-=-==-=-=-==-=-=-==-=-=-==-


    private static function bindValuesToStatement(PDOStatement $stmt, array $params)
    {
        foreach ($params as $paramName => $paramValue) {
            $stmt->bindValue($paramName, $paramValue);
        }
    }

    /**
     * Coleta resultados de queries SELECT e INSERTs com RETURNING
     */
    private static function getResult(PDOStatement $stmt, string $tableName)
    {
        $className = ClassTableNameConverter::tableNameToClassName($tableName);

        $list = [];
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            $modelObj = new $className();

            foreach ($row as $prop => $value) {
                $modelObj->__set($prop, $value);
            }
            $list[] = $modelObj;
        }
        return $list;
    }

    /**
     * Carrega o objeto do banco de dados com base no seu ID.
     *
     * @return mixed O objeto carregado do banco ou false se não for encontrado.
     */
    public static function load($object)
    {
        $pdo = self::connect();

        try {
            $tableName = $object::getTableName();

            $stmt = $pdo->prepare("SELECT * FROM $tableName WHERE " . $object->getCondition());
            $stmt->execute();

            $result = $stmt->fetch(PDO::FETCH_ASSOC);

            if ($result) {
                foreach ($result as $prop => $value) {
                    $object->__set($prop, $value);
                }
                return $object;
            }
            return false;
        } catch (PDOException $e) {
            return $e->getMessage();
        } finally {
            self::disconnect($pdo, $stmt);
        }
    }

    public static function findBy(string $tableName, string $column, mixed $value)
    {
        $pdo = self::connect();
        $stmt = null;

        try {

            $stmt = $pdo->prepare("SELECT * FROM $tableName WHERE $column = :value");
            $stmt->execute([":value" => $value]);

            $list = self::getResult($stmt, $tableName);

            return $list[0] ?? null;
        } catch (PDOException $e) {
            return $e->getMessage();
        } finally {
            self::disconnect($pdo, $stmt);
        }
    }

    /**
     * Carrega objetos do banco de dados com base nas condições fornecidas.
     * 
     * @param array $options Array com as colunas a serem retornadas e as condições
     * @return mixed Os objetos carregados do banco que correspondem à condição.
     */
    public static function findMany(string $tableName, array $options = [])
    {
        $pdo = self::connect();
        $stmt = null;
        try {

            ["sql" => $sql, "whereParams" => $whereParams, "inParams" => $inParams] = SqlQueryBuilder::buildQuery($tableName, $options);

            $stmt = $pdo->prepare($sql);
            static::bindValuesToStatement($stmt, array_merge($whereParams, $inParams));

            $stmt->execute();

            $list = self::getResult($stmt, $tableName);

            return $list;
        } catch (PDOException $e) {
            return $e->getMessage();
        } finally {
            self::disconnect($pdo, $stmt);
        }
    }

    /**
     * Retorna todos os objetos do banco de dados.
     *
     * @return mixed Todos os objetos carregados do banco.
     */
    public static function findAll(string $tableName)
    {
        $pdo = self::connect();

        try {
            $stmt = $pdo->query("SELECT * FROM $tableName ORDER BY id");

            $list = self::getResult($stmt, $tableName);
            return $list;
        } catch (Exception $e) {
            return $e->getMessage();
        } finally {
            self::disconnect($pdo, $stmt);
        }
    }

    /**
     * Permite a execução de qualquer query.
     * 
     * @param string $sql query a ser executada
     * @param array $params binding values a serem relacionados na execução da query
     *
     * @return mixed rawList de resultados 
     */
    public static function executeQuery($sql, $params = [], $returnRowCount = false)
    {
        $pdo = self::connect();

        try {
            $stmt = $pdo->prepare($sql);
            self::bindValuesToStatement($stmt, $params);

            $stmt->execute();

            if ($returnRowCount) return $stmt->rowCount();

            $rawList = [];
            while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $rawList[] = $row;
            }
            return $rawList;
        } catch (PDOException $e) {
            return $e->getMessage();
        } finally {
            self::disconnect($pdo, $stmt);
        }
    }
}
