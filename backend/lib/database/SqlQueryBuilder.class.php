<?php

class SqlQueryBuilder
{


    //=-=-=-=-=-=-=-=-=-=//=-=-=-=-=- Query INSERT -=-=-=-=-=//=-=-=-=-=-=-=-=-=-=//=-=-=-=-=-=-=-=-=-=


    public static function buildInsertQuery(string $tableName, array $attributes): array
    {
        $columns = [];
        $placeholders = [];
        $values = [];

        foreach ($attributes as $key => $value) {
            $columnName = ClassTableNameConverter::attrToColumnName($key);
            $columns[] = $columnName;
            $placeholders[] = ':' . $columnName;
            $values[":$columnName"] = $value;
        }

        $columnList = implode(', ', $columns);
        $placeholderList = implode(', ', $placeholders);

        return [
            "sql" => "INSERT INTO $tableName ($columnList) VALUES ($placeholderList)",
            "values" => $values
        ];
    }

    public static function buildInsertMultipleRowsQuery(string $tableName, array $attributes, array $list): array
    {

        $attributes = array_map('ClassTableNameConverter::attrToColumnName', array_keys($attributes));

        $placeholders = array_fill(0, count($list), '(' . implode(', ', array_fill(0, count($attributes), '?')) . ')');

        $values = [];
        foreach ($list as $object) {
            $values = array_merge($values, array_values($object->getPublicAttributes()));
        }

        return [
            "sql" => "INSERT INTO $tableName (" . implode(', ', $attributes) . ") VALUES " . implode(', ', $placeholders),
            "values" => $values
        ];
    }


    //=-=-=-=-=-=-=-=-=-=//=-=-=-=-=- Query UPDATE -=-=-=-=-=//=-=-=-=-=-=-=-=-=-=//=-=-=-=-=-=-=-=-=-=


    public static function buildUpdateQuery(string $tableName, array $attributes, string $condition): array
    {
        $updateColumns = [];
        $params = [];

        foreach ($attributes as $key => $value) {
            if ($key == 'createdAt') {
                continue;
            }

            $columnName = ClassTableNameConverter::attrToColumnName($key);
            $updateColumns[] = "$columnName = :$columnName";
            $params[":$columnName"] = $value;
        }

        $columnListStr = implode(', ', $updateColumns);

        return [
            "sql" => "UPDATE $tableName SET $columnListStr WHERE $condition",
            "params" => $params
        ];
    }


    //=-=-=-=-=-=-=-=-=-=//=-=-=-=-=- Query SELECT -=-=-=-=-=//=-=-=-=-=-=-=-=-=-=//=-=-=-=-=-=-=-=-=-=


    public static function buildWhereClause($condition)
    {
        $params = [];
        $differentValues = $condition["not"] ?? [];

        $where = [];

        if (!empty($differentValues)) {
            foreach ($differentValues as $columnDifferent => $value) {
                $columnName = ClassTableNameConverter::attrToColumnName($columnDifferent);

                $paramName = ":not$columnName";
                $where[] = "$columnName != $paramName";
                $params[$paramName] = $value;
            }
        }
        unset($condition["not"]);

        if (!empty($condition)) {
            foreach ($condition as $column => $value) {
                $columnName = ClassTableNameConverter::attrToColumnName($column);

                $paramName = ":$columnName";
                $where[] = "$columnName = $paramName";
                $params[$paramName] = $value;
            }
            $whereClause = "WHERE " . implode(" AND ", $where);
        }

        $whereClause = !empty($params) ? "WHERE " . implode(" AND ", $where) : "";


        return [
            "whereClause" => $whereClause,
            "whereParams" => $params
        ];
    }

    public static function buildInClause($in)
    {
        $inClause = "";
        $params = [];

        if (!empty($in)) {
            foreach ($in as $column => $values) {
                if (is_array($values) && !empty($values)) {
                    $inParams = [];
                    foreach ($values as $index => $value) {
                        $paramName = ":" . $column . "inParam$index";
                        $inParams[] = $paramName;
                        $params[$paramName] = $value;
                    }
                    $columnName = ClassTableNameConverter::attrToColumnName($column);
                    $inClause .= ($inClause !== "") ? " AND " : "";
                    $inClause .= "$columnName IN (" . implode(", ", $inParams) . ")";
                }
            }
        }

        return [
            "inClause" => $inClause,
            "inParams" => $params
        ];
    }

    public static function buildConditionsClause($conditions)
    {
        $where = $conditions["where"] ?? [];
        $in = $conditions["in"] ?? [];

        $whereClause = "";
        $whereParams = [];
        $inParams = [];
        if (!empty($where)) ["whereClause" => $whereClause, "whereParams" => $whereParams] = static::buildWhereClause($where);
        if (!empty($in)) {
            ["inClause" => $inClause, "inParams" => $inParams] = static::buildInClause($in);
            $whereClause .= empty($whereClause) ?
                "WHERE $inClause" :
                " AND $inClause";
        }

        return [
            "conditions" => $whereClause,
            "whereParams" => $whereParams,
            "inParams" => $inParams
        ];
    }

    private static function formatSelectColumnNames($select)
    {
        function convertField($field)
        {
            return ClassTableNameConverter::attrToColumnName(trim($field));
        }

        $fields = explode(',', $select);
        $convertedFields = array_map('convertField', $fields);

        return implode(', ', $convertedFields);
    }

    /**
     * Monta uma query select, que pode ser parametrizada com:
     *      - As colunas a serem retornadas na consulta
     *      - WHERE statement
     *      - IN statement
     *      - WHERE + IN statement
     * 
     * @param string $tableName
     * @param array $options Array com as colunas a serem retornadas e as condições
     *  
     * Exemplo de options:
     * 
     *  $options1 = [
     *      "select" => "id, name, email", // Colunas que deseja selecionar
     *      "where" => [
     *          "name" => "John Doe",
     *          "age" => 30,
     *          "status" => "active",
     *      ],
     *      "orderBy" => "dataInicio",
     *      "orderDirection" => "DESC"
     *  ];
     * 
     *  $options2 = [
     *      "select" => "id, nome" 
     *      "in" => [
     *          "tipo" => ["PESQUISA", "ENSINO"],
     *      ],
     *  ];
     * 
     *  $options3 = [
     *      "select" => "id, nome" 
     *      "in" => [
     *          "tipo" => ["PESQUISA", "ENSINO"],
     *      ],
     *      "where" => ["situacao" => "EM EXECUÇÃO"]
     *  ];
     * 
     * * Obs 1.: caso não passar nada, o método funciona como um findAll.
     * * Obs 2.: pode-se não passar nenhuma opção e selecionar somente os campos desejados 
     * *         através do índice "select".
     * 
     * @return array [
     *          "sql" => query a ser executada
     *          "whereParams" => array de parâmetros a serem associados no WHERE do prepared statement
     *          "inParams" => array de parâmetros a serem associados no IN do prepared statement
     *         ]
     */
    public static function buildQuery(string $tableName, array $options): array
    {

        $columns = isset($options["select"]) ? self::formatSelectColumnNames($options["select"]) : "*";

        $orderBy = $options["orderBy"] ?? "";
        $orderDirection = $options["orderDirection"] ?? "";
        $limit = $options["limit"] ?? "";

        ["conditions" => $whereClause, "whereParams" => $whereParams, "inParams" => $inParams] = static::buildConditionsClause($options);

        // Adicione a cláusula ORDER BY na consulta
        $orderByColumn = ClassTableNameConverter::attrToColumnName($orderBy);
        $orderByClause = $orderBy !== "" ? "ORDER BY $orderByColumn $orderDirection" : "";
        $limitClause = $limit !== "" ? "LIMIT $limit" : "";

        return [
            "sql" => "SELECT $columns FROM $tableName $whereClause $orderByClause $limitClause",
            "whereParams" => $whereParams,
            "inParams" => $inParams
        ];
    }

    /**
     * Retorna a página de objetos solicitada.
     * 
     * Deve obrigatoriamente receber a página e o número de linhas a serem obtidas.
     * 
     * Pode opcionalmente receber:
     *      - filtros de busca para uma cláusula WHERE
     *      - campo a ser utilizado na cláusula ORDER BY
     *      - ordem a ser utilizada na cláusula ORDER BY [ASC | DESC] (ASC por padrão)
     *
     * @return array Todos os objetos carregados do banco.
     */
    public static function buildPaginatedQuery(string $tableName, array $options): array
    {
        $columns = isset($options["select"]) ? self::formatSelectColumnNames($options["select"]) : "*";

        $filters = $options["filters"] ?? [];
        $orderBy = $options["orderBy"] ?? "id";
        $orderDirection = $options["orderDirection"] ?? "ASC";

        ["conditions" => $whereClause, "whereParams" => $whereParams, "inParams" => $inParams] = static::buildConditionsClause($filters);

        // Adicione a cláusula ORDER BY na consulta
        $orderByColumn = ClassTableNameConverter::attrToColumnName($orderBy);
        $orderByClause = "ORDER BY $orderByColumn $orderDirection";

        return [
            "sql" => "SELECT $columns FROM $tableName $whereClause $orderByClause LIMIT :limit OFFSET :offset",
            "whereParams" => $whereParams,
            "inParams" => $inParams
        ];
    }
}
