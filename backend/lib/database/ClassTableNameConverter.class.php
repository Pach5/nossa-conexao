<?php

class ClassTableNameConverter
{
    //=-=-=-=-=-=-=-=-=-=//=-=-=-=-=- Conversão de nomenclatura classe-tabela =-=-=-=-=-=-=-=-=-=//=-=-=-=-=-=-=-=-=-=


    /**
     * Converte snake_case (colunas do banco) para camelCase (atributos da classe)
     * 
     ** Dados vindos do banco
     */
    public static function columnNameToAttr($prop)
    {
        if (!preg_match('/^[a-z]+(?:_[a-z]+)*$/', $prop)) {
            return $prop;
        }

        $words = explode('_', strtolower($prop));
        $camelCaseString = '';

        foreach ($words as $word) {
            $camelCaseString .= ucfirst($word);
        }

        return lcfirst($camelCaseString);
    }

    /**
     * Converte snake_case (nome de tabela do banco) para PascalCase (nome de classe)
     * 
     ** Dados vindos do banco 
     */
    public static function tableNameToClassName($snakeCaseString)
    {
        $pascalCaseString = preg_replace_callback('/(?:^|_)(.?)/', function ($match) {
            return strtoupper($match[1]);
        }, $snakeCaseString);

        return $pascalCaseString;
    }

    /**
     * Converte camelCase (atributos da classe) para snake_case (colunas do banco)
     * 
     ** Dados indo para o banco
     */
    public static function attrToColumnName($attribute)
    {
        return strtolower(preg_replace('/(?<!^)[A-Z]/', '_$0', $attribute));
    }
}
