<?php

trait Model
{

    private $_id;
    private $_condition;

    public function getId()
    {
        return $this->_id;
    }

    public function setId($id)
    {
        $this->_id = $id;
    }

    public function getCondition()
    {
        return $this->_condition ? $this->_condition : "id = " . $this->_id;
    }

    public function setCondition($condition)
    {
        $this->_condition = $condition;
    }

    /**
     * Identificar joins para settar id do último objeto
     * salvo somente se ele não for um join.
     * 
     * Solução mais barata, considerando que são poucas 
     * classes.
     */
    public abstract static function isJoinTable(): bool;

    public abstract static function getTableName(): string;

    public function __get($prop)
    {
        $method = "get" . ucfirst($prop);
        if (method_exists($this, $method)) {
            return $this->{$method}();
        }
        return $this->{$prop};
    }

    public function __set($prop, $value)
    {
        $method = "set" . ucfirst($prop);
        if (method_exists($this, $method)) {
            $this->{$method}($value);
            return;
        }
        $prop = ClassTableNameConverter::columnNameToAttr($prop);
        $this->{$prop} = $value;
    }

    public static function __callStatic($method, $args)
    {
        $class = self::class;
        $object = new $class();
        return $object->{$method}($args);
    }

    public function jsonSerialize(): mixed
    {
        $data = [];

        foreach ($this as $prop => $value) {
            $data[$prop] = $value;
        }

        return $data;
    }

    public static function setResultList($dados)
    {
        if (empty($dados) || !is_array($dados))
            return;

        $className = get_called_class();
        $lista = [];
        foreach ($dados as $row) {
            $modelObj = new $className();
            foreach ($row as $prop => $value) {
                $modelObj->__set($prop, $value);
            }
            $lista[] = $modelObj;
        }
        return $lista;
    }

    /**
     * Retorna array com os nomes dos atributos que devem ser 
     * salvos no banco
     * 
     ** Obs.: os que não devem ser salvos são diferenciados pelo prefixo "_"
     ** (geralmente ids e atributos que são objetos de outras classes)
     */
    public function getPublicAttributes()
    {
        $attributes = get_object_vars($this);
        return array_filter($attributes, function ($key) {
            return substr($key, 0, 1) !== "_";
        }, ARRAY_FILTER_USE_KEY);
    }

    public function save()
    {
        return DBManager::save($this);
    }

    public function lazyLoad()
    {
        return DBManager::load($this);
    }

    public static function findBy(string $column, mixed $value)
    {
        return DBManager::findBy(self::getTableName(), $column, $value);
    }

    public static function findAll()
    {
        return DBManager::findAll(self::getTableName());
    }

    public static function findMany($options)
    {
        return DBManager::findMany(self::getTableName(), $options);
    }

    public function update()
    {
        return DBManager::update($this);
    }

    public function delete()
    {
        return DBManager::delete($this);
    }
}