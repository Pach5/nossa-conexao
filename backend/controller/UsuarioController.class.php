<?php

class UsuarioController extends AbstractController
{
    private $model = Usuario::class;

    public function login()
    {
        if ($_SERVER["REQUEST_METHOD"] === "OPTIONS") {
            return $this->response("Ok.", 200);
        }

        if (!in_array($_SERVER["REQUEST_METHOD"], ["POST", "GET"])) {
            return $this->methodNotAllowedResponse();
        }

        $data = $this->getRequestBody();
        if (!isset($data)) return $this->badRequestResponse();

        $usuario = Usuario::findBy("username", $data["username"]);
        if ($usuario == null) return $this->notFoundResponse("Usuário não encontrado!");

        return $this->response(AuthManager::authenticate($usuario, $data["senha"]));
    }

    private function isUsernameInUse()
    {
        $data = $this->getRequestBody();
        if (!isset($data)) return $this->badRequestResponse();

        $username = Usuario::findBy("username", $data["username"]);
        return $username !== null;
    }

    public function createUsuario()
    {
        if ($_SERVER["REQUEST_METHOD"] === "OPTIONS") {
            return $this->response("Ok.", 200);
        }

        if ($_SERVER["REQUEST_METHOD"] !== "POST") {
            return $this->methodNotAllowedResponse();
        }

        $data = $this->getRequestBody();
        if (!isset($data)) return $this->badRequestResponse();

        if ($this->isUsernameInUse($data["username"])) {
            return $this->response("Nome de usuário já está em uso!", 400);
        }

        $usuario = new Usuario();
        $usuario->__set("username", $data["username"]);
        $usuario->__set("role", "user");

        $salt = Crypto::generateSalt();
        $hashedPassword = Crypto::hashPassword($data["senha"], $salt);
        $usuario->__set("senha", $hashedPassword);
        $usuario->__set("salt", $salt);

        if ($usuario->save()) {
            $usuario->__set("senha", null);
            $usuario->__set("salt", null);
        }

        return $this->response($usuario, 201);
    }

    public function updateUsuario()
    {
        if ($_SERVER["REQUEST_METHOD"] === "OPTIONS") {
            return $this->response("Ok.", 200);
        }

        $data = $this->getRequestBody();
        if (!isset($data)) {
            return $this->badRequestResponse();
        }

        return $this->update($this->model, $data);
    }

    public function deleteUsuario()
    {
        if ($_SERVER["REQUEST_METHOD"] === "OPTIONS") {
            return $this->response("Ok.", 200);
        }

        $data = $this->getRequestBody();
        if (!isset($data)) {
            return $this->badRequestResponse();
        }

        return $this->delete($this->model, $data);
    }

    public function findOneUsuario()
    {
        if ($_SERVER["REQUEST_METHOD"] === "OPTIONS") {
            return $this->response("Ok.", 200);
        }

        $data = $this->getRequestBody();
        if (!isset($data)) {
            return $this->badRequestResponse();
        }

        return $this->findOne($this->model, $data);
    }

    public function findAllUsuarios()
    {
        if ($_SERVER["REQUEST_METHOD"] === "OPTIONS") {
            return $this->response("Ok.", 200);
        }

        return $this->findAll($this->model);
    }
}
