<?php

class ArtistaController extends AbstractController
{

    private $model = Artista::class;


    public function createArtista()
    {
        if ($_SERVER["REQUEST_METHOD"] === "OPTIONS") {
            return $this->response("Ok.", 200);
        }

        $data = $this->getRequestBody();
        if (!isset($data)) return $this->badRequestResponse();

        // if (!$this->verifySession()) return $this->unauthorizedResponse();

        return $this->create($this->model, $data);
    }

    public function updateArtista()
    {
        if ($_SERVER["REQUEST_METHOD"] === "OPTIONS") {
            return $this->response("Ok.", 200);
        }

        $data = $this->getRequestBody();
        if (!isset($data)) {
            return $this->badRequestResponse();
        }

        return $this->update($this->model, $data);
    }

    public function deleteArtista()
    {
        if ($_SERVER["REQUEST_METHOD"] === "OPTIONS") {
            return $this->response("Ok.", 200);
        }

        $data = $this->getRequestBody();
        if (!isset($data)) {
            return $this->badRequestResponse();
        }

        return $this->delete($this->model, $data);
    }

    public function findOneArtista()
    {
        if ($_SERVER["REQUEST_METHOD"] === "OPTIONS") {
            return $this->response("Ok.", 200);
        }

        $data = $this->getRequestBody();
        if (!isset($data)) {
            return $this->badRequestResponse();
        }

        return $this->findOne($this->model, $data);
    }

    public function findAllArtistas()
    {
        return $this->findAll($this->model);
    }
}
