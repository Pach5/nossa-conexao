<?php

class MusicaController extends AbstractController
{

    private $model = Musica::class;


    public function createMusica()
    {
        if ($_SERVER["REQUEST_METHOD"] === "OPTIONS") {
            return $this->response("Ok.", 200);
        }

        $data = $this->getRequestBody();
        if (!isset($data)) return $this->badRequestResponse();

        // if (!$this->verifySession()) return $this->unauthorizedResponse();

        return $this->create($this->model, $data);
    }

    public function updateMusica()
    {
        if ($_SERVER["REQUEST_METHOD"] === "OPTIONS") {
            return $this->response("Ok.", 200);
        }

        $data = $this->getRequestBody();
        if (!isset($data)) {
            return $this->badRequestResponse();
        }

        return $this->update($this->model, $data);
    }

    public function deleteMusica()
    {
        if ($_SERVER["REQUEST_METHOD"] === "OPTIONS") {
            return $this->response("Ok.", 200);
        }

        if ($_SERVER["REQUEST_METHOD"] !== "POST") {
            return $this->methodNotAllowedResponse();
        }

        $data = $this->getRequestBody();
        if (!isset($data)) {
            return $this->badRequestResponse();
        }

        $result = DBManager::executeQuery(
            "DELETE m, a1, a2 FROM musica m LEFT JOIN arquivo a1 ON m.arquivo_id = a1.id LEFT JOIN arquivo a2 ON m.foto_id = a2.id WHERE m.id = :idMusica",
            [":idMusica" => $data["id"]],
            true
        );

        return $this->response($result);
    }

    public function findOneMusica()
    {
        if ($_SERVER["REQUEST_METHOD"] === "OPTIONS") {
            return $this->response("Ok.", 200);
        }

        $data = $this->getRequestBody();
        if (!isset($data)) {
            return $this->badRequestResponse();
        }

        $rawList = DBManager::executeQuery(
            "SELECT m.id, m.titulo, m.texto, a.id AS artistaId, a.nome AS artista, ar_audio.id AS arquivoId, ar_audio.path AS audio, ar_imagem.id AS fotoId, ar_imagem.path AS imagem FROM musica m LEFT JOIN artista a ON m.artista_id = a.id LEFT JOIN usuario u ON m.usuario_id = u.id LEFT JOIN arquivo ar_audio ON m.arquivo_id = ar_audio.id LEFT JOIN arquivo ar_imagem ON m.foto_id = ar_imagem.id WHERE m.id = :idMusica",
            [":idMusica" => $data["id"]]
        );
        return empty($rawList) ? $this->notFoundResponse() : $this->response($rawList[0]);
    }

    public function findAllMusicas()
    {
        if ($_SERVER["REQUEST_METHOD"] === "OPTIONS") {
            return $this->response("Ok.", 200);
        }

        if ($_SERVER["REQUEST_METHOD"] !== "GET") {
            return $this->methodNotAllowedResponse();
        }

        $rawList = DBManager::executeQuery(
            "SELECT m.id AS _id, m.titulo, m.texto, a.nome AS artista, u.username, ar_audio.path AS audio, ar_imagem.path AS imagem FROM musica m LEFT JOIN artista a ON m.artista_id = a.id LEFT JOIN usuario u ON m.usuario_id = u.id LEFT JOIN arquivo ar_audio ON m.arquivo_id = ar_audio.id LEFT JOIN arquivo ar_imagem ON m.foto_id = ar_imagem.id ORDER BY _id"
        );
        return $this->response($rawList);
    }
}
