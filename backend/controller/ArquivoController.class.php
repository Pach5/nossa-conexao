<?php

class ArquivoController extends AbstractController
{
    private function uploadArquivo($inputName, $allowedTypes, $savePath)
    {
        if ($_SERVER["REQUEST_METHOD"] !== "POST") {
            return $this->methodNotAllowedResponse();
        }

        if (!isset($_FILES[$inputName])) {
            return $this->badRequestResponse();
        }

        $arquivo = $_FILES[$inputName];

        if (!in_array($arquivo["type"], $allowedTypes)) {
            return $this->unsupportedMediaTypeResponse();
        }

        $fileName = date("Y-m-d_H-i-s") . "_" . $arquivo["name"];
        $dir = PUBLIC_MEDIA . $savePath;

        if (!file_exists($dir)) {
            mkdir($dir, 0777, true);
        }

        $filePath = $dir . $fileName;

        // Mover o arquivo para o diretório de destino
        if (move_uploaded_file($arquivo["tmp_name"], $filePath)) {
            $arquivoObj = new Arquivo();
            $arquivoObj->__set("nome", $fileName);
            $arquivoObj->__set("tipo", $arquivo["type"]);
            $arquivoObj->__set("tamanho", $arquivo["size"]);

            $relativePath = substr($filePath, strpos($filePath, "/media"));

            $arquivoObj->__set("path", $relativePath);
            $arquivoObj->save();

            return $this->response($arquivoObj, 201);
        } else {
            return $this->response("Falha ao salvar o arquivo.", 500);
        }
    }

    public function uploadImagem()
    {
        $allowedTypes = ["image/jpeg", "image/png", "image/jpg"];
        $savePath = "/img/";

        return $this->uploadArquivo("imagem", $allowedTypes, $savePath);
    }

    public function uploadAudio()
    {
        $allowedTypes = ["audio/mpeg", "audio/wav", "audio/mp3"];
        $savePath = "/audio/";

        return $this->uploadArquivo("audio", $allowedTypes, $savePath);
    }
}
