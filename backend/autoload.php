<?php

function my_autoload($pClassName)
{

    $mapa = [
        "DBManager" => __DIR__ . "/lib/database/DBManager.class.php",
        "ClassTableNameConverter" => __DIR__ . "/lib/database/ClassTableNameConverter.class.php",
        "SqlQueryBuilder" => __DIR__ . "/lib/database/SqlQueryBuilder.class.php",
        "Model" => __DIR__ . "/lib/model/Model.trait.php",
        "AbstractController" => __DIR__ . "/lib/controller/AbstractController.class.php",
        "Crypto" => __DIR__ . "/lib/crypto/Crypto.class.php",
        "AuthManager" => __DIR__ . "/lib/auth/AuthManager.class.php",
    ];

    if (isset($mapa[$pClassName])) {
        include_once $mapa[$pClassName];
        return;
    }

    $file = __DIR__ . "/model/" . $pClassName . ".class.php";
    if (file_exists($file)) {
        include_once $file;
        return;
    }

    $file = __DIR__ . "/controller/" . $pClassName . ".class.php";
    if (file_exists($file)) {
        include_once $file;
        return;
    }
}
spl_autoload_register("my_autoload");
