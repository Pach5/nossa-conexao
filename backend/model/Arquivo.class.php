<?php

#[\AllowDynamicProperties]
class Arquivo implements JsonSerializable
{
    use Model;

    private $nome;
    private $tipo;
    private $tamanho;
    private $path;

    public static function getTableName(): string
    {
        return "arquivo";
    }

    public static function isJoinTable(): bool
    {
        return false;
    }
}
