<?php

#[\AllowDynamicProperties]
class Artista implements JsonSerializable
{

    use Model;

    private $nome;

    public static function getTableName(): string
    {
        return "artista";
    }

    public static function isJoinTable(): bool
    {
        return false;
    }
}
