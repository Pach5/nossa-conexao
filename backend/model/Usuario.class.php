<?php

#[\AllowDynamicProperties]
class Usuario implements JsonSerializable
{

    use Model;

    private $username;
    private $senha;
    private $role;
    private $salt;
    private $ativo;

    public static function getTableName(): string
    {
        return "usuario";
    }

    public static function isJoinTable(): bool
    {
        return false;
    }
}
