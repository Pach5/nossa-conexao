<?php

#[\AllowDynamicProperties]
class Musica implements JsonSerializable
{

    use Model;

    private $titulo;
    private $texto;
    private $artistaId;
    private $arquivoId;
    private $usuarioId;
    private $fotoId;

    private $_artista;
    private $_arquivo;
    private $_usuario;
    private $_foto;

    public static function getTableName(): string
    {
        return "musica";
    }

    public static function isJoinTable(): bool
    {
        return false;
    }
}
